from transformers import AutoModelForCausalLM, AutoTokenizer

model = AutoModelForCausalLM.from_pretrained("/root/projects/test/", trust_remote_code=True)



tokenizer = AutoTokenizer.from_pretrained("EleutherAI/gpt-neo-1.3B", trust_remote_code=True)

mood_input = input("Mood: ")

inputs = tokenizer("Prompt: %s Completions: You're feeling"%mood_input, return_tensors="pt", return_attention_mask=True)

outputs = model.generate(**inputs, max_length=12)

print(tokenizer.batch_decode(outputs)[0])




